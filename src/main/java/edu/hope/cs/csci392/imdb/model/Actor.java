package edu.hope.cs.csci392.imdb.model;

import java.util.List;

import edu.hope.cs.csci392.imdb.Database;

public class Actor {
	
	private int actorID;
	private String stageFirstName;
	private String stageLastName;
	private String birthCity;
	private String birthStateOrProvince;
	private String deathCity;
	private String deathStateOrProvince;
	private int heightInInches;
	private char gender;
	
	public Actor (
		int actorID,
		String stageFirstName, String stageLastName,
		String birthCity, String birthStateOrProvince,
		String deathCity, String deathStateOrProvince,
		char gender, int heightInInches
	) {
		this.actorID = actorID;
		this.stageFirstName = stageFirstName;
		this.stageLastName = stageLastName;
		this.birthCity = birthCity;
		this.birthStateOrProvince = birthStateOrProvince;
		this.deathCity = deathCity;
		this.deathStateOrProvince = deathStateOrProvince;
		this.gender = gender;
		this.heightInInches = heightInInches;
	}

	public String getStageFirstName() {
		return stageFirstName;
	}

	public String getStageLastName() {
		return stageLastName;
	}

	public String getBirthCity() {
		return birthCity;
	}

	public String getBirthStateOrProvince() {
		return birthStateOrProvince;
	}

	public String getDeathCity() {
		return deathCity;
	}

	public String getDeathStateOrProvince() {
		return deathStateOrProvince;
	}

	public int getHeightInInches() {
		return heightInInches;
	}

	public char getGender() {
		return gender;
	}

	public int getActorID() {
		return actorID;
	}
	
	public List<Role> getRoles () {
		Database database = Database.getInstance();
		return database.findMoviesForActor(this);
	}
}